$(document).ready(function(){

	// BREAKPOINTS
	var breakpoints = [
		'screen-xs',
		'screen-sm',
		'screen-md',
		'screen-lg'
	];	

	//slider init
	    $('.slider').slick({
	 	   infinite: true,
	 	   dots: true,
	 	   autoplay: true,
  		   slidesToShow: 3,
  		   slidesToScroll: 1,
  		   nextArrow: '<svg class="icon icon-arrow-right"><use xlink:href="/img/sprite__svg/sprite.svg#arrow-right"></use></svg>',
  		   prevArrow: '<svg class="icon icon-arrow-left"><use xlink:href="/img/sprite__svg/sprite.svg#arrow-left"></use></svg>',
  		   responsive: [
		    {
		      breakpoint: 992,
		      settings: {
		        slidesToShow: 2,
		        slidesToScroll: 1,
		        infinite: true,
		        dots: true
		      }
		    },
		    {
		      breakpoint: 768,
		      settings: {
		        slidesToShow: 1,
		        slidesToScroll: 1,
		        infinite: true,
		        dots: true
		      }
		    }
		   ]

		});

	//mobile nav
		$(".mobile-nav").on("click", function(){
		   $(".mobile-nav__item").toggleClass("active-btn");
		   $(".mobile-content").toggleClass("open-container");
		   $(this).toggleClass("fixed");
		});

		$(".mobile-content__menu .menu__item-link").click(function(e) {
			var $subMenu = $(this).siblings(".menu__sub-menu"),
				$btnBack;

			if ($subMenu.length) {
				$btnBack = $subMenu.find(".menu__sub-menu-item--go-back");

				if ($btnBack.length === 0) {
					$subMenu.prepend('<li class="menu__sub-menu-item--go-back">' + $(this).text() + '</li>');
				}

				$(".mobile-content__menu .menu__item-link").addClass("closed");
				$subMenu.addClass("opened");
				e.preventDefault();
			}
		});

		$("body").on("click", ".menu__sub-menu-item--go-back", function() {
			var $subMenu = $(".menu__sub-menu.opened");

			if ($subMenu.length) {
				$(".mobile-content__menu .menu__item-link").removeClass("closed");
				$subMenu.removeClass("opened");
			}
		});

	
	//fixed menu on scroll
	var navigation_top = $(".header__menu-container");
    var offset_top_menu = navigation_top.offset().top;
        
    $(window).scroll(function () {
    	checkFixedMenu();
    });

    checkFixedMenu();

    function checkFixedMenu() {
        var scroll_top = jQuery(window).scrollTop();
        
        if (scroll_top > (offset_top_menu)) {
            navigation_top.addClass('fixed');               
        } else {
            navigation_top.removeClass('fixed');
        }

    }

	// init datepicker
    $(".js-datepicker").datepicker({
    	inline: true,
    	minDate: new Date(),
    	prevHtml: '<svg class="icon icon-datepicker-arrow-l"> \
                 <use xlink:href="img/sprite__svg/sprite.svg#datepicker-arrow-l"></use> \
               </svg>',
    	nextHtml: '<svg class="icon icon-datepicker-arrow-r"> \
                 <use xlink:href="img/sprite__svg/sprite.svg#datepicker-arrow-r"></use> \
               </svg>'
    });

	// form validation
	// $("form").validate();


	// contact form ajax
	$(document).on("submit","#contactForm",function(e){
		e.preventDefault();
		var m_method=$(this).attr('method');
		var m_action=$(this).attr('action');
		var m_data=$(this).serialize();

		$.ajax({
			type: m_method,
			url: m_action,
			data: m_data,
			resetForm: 'true',
			success: function(result){
				var data = $(result).find("#callPopup").html();
				$("#callPopup").html(data);
				console.log(data);
			}
		});
	});


	// fancybox
	// $(".js-fancybox").fancybox();

	// reviews slider
	$(".reviews-list").slick({
		dots: true,
		infinite: false,
		speed: 600,
		slidesToShow: 3,
		slidesToScroll: 1,
		adaptiveHeight: true,
		responsive: [
			{
				breakpoint: 992,
				settings: {
					slidesToShow: 1
				}
			}
		]
	});

	// portfolio slider
	$(".portfolio-list").slick({
		dots: true,
		infinite: false,
		speed: 600,
		slidesToShow: 5,
		slidesToScroll: 3,
		responsive: [
			{
				breakpoint: 1024,
				settings: {
					slidesToShow: 3
				}
			},			
			{
				breakpoint: 480,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1,
					dots: false
				}
			}
		]
	});


	// graduates grid
	var $graduatesList = $('.graduates-list');

	if ($graduatesList.length) {
		$graduatesList.masonry({
			itemSelector: '.graduates-list__item',
			percentPosition: true
		});

		$(window).on("load",function(){
			$graduatesList.masonry();
		});

	}


	// contacts map position
	var $map = $("#map");

	if ($map.length) {
		$(window).resize(function() {
			setPosMap();
		});

		setPosMap();

		function setPosMap() {
			var $contactsInfoBlock = $(".contacts-section__col-left"),
				currentBreakpoint = breakpoint.value;

			if (breakpoints.indexOf(currentBreakpoint) > breakpoints.indexOf('screen-sm')) {
				$("#map").css({
					'left': $contactsInfoBlock.offset().left + $contactsInfoBlock.outerWidth() + 20,
					'width': 'auto'
				});
			} else {
				$("#map").css({
					'left': 0
				});
			}
		}
	}

	//cource item hover
	$(".products-list__inner").hover(function() {
		var $title = $(this).find(".products-list__title"),
			widthItem = $(this).outerWidth(),
			widthTitle = $title.outerWidth();

		$title.css({
			"transform": "translateX(" + -(widthItem - widthTitle)/2 + "px)"
		});
	}, function() {
		var $title = $(this).find(".products-list__title");

		$title.css({
			"transform": "none"
		});
	});


});

//disable open
// $(document).mouseup(function (e) {
//     var container = $('.menu__item-btn, .menu__sub-menu');
// 	    if (container.has(e.target).length === 0){
// 	     container.removeClass('open');
// 	     container.removeClass('clicked')
// 	    }
// 	   });


	