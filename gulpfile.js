// VAR
var 	gulp           		= require('gulp'), 						// GULP
		gutil          		= require('gulp-util'), 				// Різні допоміжні утиліти
		plumber		   		= require('gulp-plumber'),				// Linter
	    sass           		= require('gulp-sass'),					// Препроцессор Sass
		// pug	   		   		= require('gulp-pug'),					// Препроцессор Pug
		// data 		   		= require('gulp-data'),					// Pug-json
		//merge 				= require('gulp-merge-json'),			// Конкатенація JSON
		browserSync    		= require('browser-sync'),				// Автоперезагрузка браузера
		concat        	 	= require('gulp-concat'),				// Конкатенація (з'єднання) файлів
		uglify         		= require('gulp-uglify'),				// Стиснення JS
		cleanCSS       		= require('gulp-clean-css'),			// Стиснення CSS
		rename         		= require('gulp-rename'),				// Для перейменування файлів
		del            		= require('del'),						// Для видалення файлів і папок
		cache          		= require('gulp-cache'),				// Кешування зображень
		autoprefixer   		= require('gulp-autoprefixer'),			// Автоматичне додавання префіксів
		ftp            		= require('vinyl-ftp'),					// Deploy FTP
		imagemin       		= require('gulp-imagemin'),				// Оптимізація зображень
		pngquant       		= require('imagemin-pngquant'),			// TinyPNG
		spritesmith    		= require('gulp.spritesmith'),			// Спрайт PNG

	// SVG SPRITE
		svgSprite 	   		= require('gulp-svg-sprite'),			// Спрайт SVG
		replace   			= require('gulp-replace'),				// fix SVG
		svgmin    	   		= require('gulp-svgmin'),				// SVGO оптимізація

	// Directory
		assetsDir 	   		= 'app/', 								// Папка DEV
		buildDir 	   		= 'production/',						// Папка PRODUCTION
		modxDir				= 'backend';							// Папка Modx

// =============================================================================
// Перехват помилок
// =============================================================================

	var err = {
		errorHandler: function (error) {
			gutil.log('Error: ' + error.message);
			gutil.beep();
			this.emit('end');
		}
	}


// =============================================================================
// Reload data JSON
// =============================================================================

	function requireUncached( $module ) {
		delete require.cache[require.resolve( $module )];
		return require( $module );
	}

// =============================================================================
// BrowserSync
// =============================================================================

	gulp.task('browser-sync', function() {
		browserSync({
			server: {
				baseDir: assetsDir
			},
			notify: false // скриваємо повідомлення про підключення browserify
		});
	});


// =============================================================================
// JSON merge
// =============================================================================


	gulp.task('jsonAll', function() {
		return gulp.src("./app/_pugfiles/json/*.json")
			.pipe(plumber(err))
			.pipe(merge("main.json"))
			.pipe(gulp.dest(assetsDir + "_pugfiles/jsonmin/"))
	});



// =============================================================================
// Pugs
// =============================================================================

	gulp.task('pugs', function() {
		//var dataFile = './app/_pugfiles/jsonmin/main.json';
		return gulp.src(assetsDir + '_pugfiles/**/!(_)*.pug')
		.pipe(plumber(err))
		//.pipe(data(function(file) {return JSON.parse(fs.readFileSync(dataFile));}))
		//.pipe(data(function(file) { return requireUncached(dataFile); }))
		.pipe(pug({pretty: true}))
		.pipe(gulp.dest(assetsDir))
		// .pipe(browserSync.stream())
		.pipe(browserSync.reload({stream: true}))
	});


// =============================================================================
// Sass
// =============================================================================

gulp.task('sass', function() {
	return gulp.src(assetsDir + 'sass/**/*.scss')
		.pipe(sass({
			outputStyle: 'compressed'
		}).on('error', sass.logError))
		.pipe(rename({suffix: '.min', prefix : ''}))
        .pipe(autoprefixer({
            browsers: ['last 2 versions', 'safari 5', 'ie 11', 'opera 12.1', 'ios 6', 'android 4'],
            cascade: false
        }))
		.pipe(cleanCSS())
		.pipe(gulp.dest('app/css'))
		.pipe(browserSync.reload({stream: true})) //Інжектить код, без перезагрузки
});

// =============================================================================
// SVG SPRITE
// =============================================================================

var baseSVG = {
		mode: {
			symbol: {
				sprite: "../sprite.svg",
				render: {
					scss: {
						dest:'../../../sass/icons/_sprite-svg.scss',
						template: assetsDir + "sass/icons/templates/_sprite_template.scss"
					}
				}
			}
		},
	};

gulp.task('svgSprite', function () {
	return gulp.src(assetsDir + 'img/svg/*.svg')
		.pipe(svgmin({
			plugins: [{
				removeStyleElement: true
			},{ removeAttrs: { 
					attrs: '(preserveAspectRatio|viewBox|class|xmlns|width|height)',
				}
			}],
		}))
		// build svg sprite
		.pipe(replace('&gt;', '>'))
		.pipe(svgSprite(baseSVG))
		.pipe(gulp.dest(assetsDir + 'img/sprite__svg/'));
});


// =============================================================================
// PNG SPRITE
// =============================================================================

gulp.task('pngSprite', function () {
    var spriteData = gulp.src(assetsDir + 'img/png/*.png')
        .pipe(spritesmith({
			imgName: '../img/sprite__png/sprite.png',
			cssName: 'icons/_sprite-png.scss',
			cssFormat: 'scss',
			algorithm: 'binary-tree',
			imgPath: '../../img/sprite__png/sprite.png',
			padding: 1,
			cssVarMap: function(sprite) {
				sprite.name = 'icon-' + sprite.name
			}//
        }));
    spriteData.css.pipe(gulp.dest(assetsDir + 'sass/'));
    spriteData.img.pipe(gulp.dest(assetsDir + 'img/'));
});

// =============================================================================
// Оптимізація IMAGES
// =============================================================================
gulp.task('imagemin', function() {
	return gulp.src([assetsDir + 'img/**/*', '!' + assetsDir + 'img/pdf{,/**}', '!' + assetsDir + 'img/png{,/**}', '!' + assetsDir + 'img/svg{,/**}', '!' + assetsDir + 'img/sprite__svg{,/**}', '!' + assetsDir + 'img/favicon{,/**}'])
		.pipe(cache(imagemin({
			interlaced: true,
			progressive: true,
			svgoPlugins: [{removeViewBox: false}],
			use: [pngquant()]
		})))
		.pipe(gulp.dest(buildDir + 'img'));
});

// =============================================================================
// Мініфікація JS
// =============================================================================

gulp.task('minjs', function() {
	return gulp.src([
			// assetsDir + 'js/**/*.js'
			assetsDir + 'js/all.js'
		])
		.pipe(uglify())
		.pipe(gulp.dest(buildDir + 'js/'));
});

// =============================================================================
// Конкатенація JS
// =============================================================================

gulp.task('concatjs', function() {
	return gulp.src([
			assetsDir + 'js/all/**/*.js'
		])
		.pipe(concat('all.js'))
		.pipe(gulp.dest(assetsDir + 'js/'));
});

// =============================================================================
// GULP WATHER
// =============================================================================

gulp.task('watch', ['sass', 'concatjs', 'browser-sync'], function() {
	gulp.watch(assetsDir + 'img/**/*');
	gulp.watch(assetsDir + 'sass/**/*.scss', ['sass']);
	// gulp.watch(assetsDir + '_pugfiles/json/**/*.json', ['pugs']);
	//gulp.watch(assetsDir + '_pugfiles/**/*.pug', ['pugs']);
	gulp.watch(assetsDir + 'js/**/*.js', ['concatjs', browserSync.reload]);
	gulp.watch(assetsDir+ '*.html', browserSync.reload);
});

// =============================================================================
// GULP BUILD
// =============================================================================

gulp.task('build', ['clean', 'imagemin', 'sass', 'minjs'], function() {
	gulp.src(assetsDir + 'css/**/*.css').pipe(gulp.dest(buildDir + 'css'));
	gulp.src(assetsDir + '.htaccess').pipe(gulp.dest(buildDir));
	gulp.src(assetsDir + '*.html').pipe(gulp.dest(buildDir));
	gulp.src(assetsDir + 'fonts/**/*').pipe(gulp.dest(buildDir +'fonts'));
	gulp.src(assetsDir + 'video/**/*').pipe(gulp.dest(buildDir + 'video'));
	gulp.src(assetsDir + 'img/sprite__svg/**/*').pipe(gulp.dest(buildDir + 'img/sprite__svg'));
	gulp.src(assetsDir + 'img/favicon/**/*').pipe(gulp.dest(buildDir + 'img/favicon'));
});



// =============================================================================
// GULP modx
// =============================================================================

gulp.task('modx', function() {
	gulp.src([buildDir + '/**/*', buildDir + '/.*', '!'+ buildDir + '/*.html']).pipe(gulp.dest(modxDir));
});



// =============================================================================
// GULP DEPLOY
// =============================================================================

gulp.task('deploy', function() {

	var conn = ftp.create({
		host:      'hostname.com',
		user:      'username',
		password:  'userpassword',
		parallel:  10,
		log: gutil.log
	});

	var globs = [
	'dist/**',
	'dist/.htaccess',
	];
	return gulp.src(globs, {buffer: false})
	.pipe(conn.dest('/path/to/folder/on/server'));
});

// CLEAN
gulp.task('clean', function() {return del.sync(buildDir)});

// clearCache
gulp.task('clearCache', function () { return cache.clearAll(); });

// =============================================================================
// GULP RUN
// =============================================================================
gulp.task('default', ['watch']);
